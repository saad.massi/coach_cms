<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::create([
                'name'=>"ADMIN",
                /* 'role'=>"ADMIN", */
                'email'=>env('DEFAULT_EMAIL'),
                'email_verified_at'=>date("Y-m-d h:i:s"),
                'password'=>bcrypt(env('DEFAULT_PASSWORD'))
            ]);
    }
}
