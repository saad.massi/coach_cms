<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(50)->create();
        \App\Models\MealPlan::factory(120)->create();
        \App\Models\WorkoutPlan::factory(120)->create();
        \App\Models\UserWeight::factory(120)->create();
    }
}
