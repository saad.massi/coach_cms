<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Seeder;

class MealPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\MealPlan::create([
            'name'    => $this->faker->name(),
            'body'    => json_encode([$this->faker->name() => $this->faker->paragraph()]),
            'user_id' => $this->faker->numberBetween(1,50),
        ]);
    }
}
