<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class MealPlanFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name'    => $this->faker->name(),
            //'body'    => $this->faker->paragraph(),
            'body'    =>json_encode([$this->faker->name() => $this->faker->paragraph()]),
            'user_id' => $this->faker->numberBetween(1,50),
        ];
    }
}
