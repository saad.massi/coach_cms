<x-filament::page class="grid-cols-2">
 
<div class="grid gap-4 lg:gap-8 filament-stats md:grid-cols-3">
    <x-filament::card class="p-6">
        <x-filament::card.heading class="flex items-center space-x-2 rtl:space-x-reverse text-sm font-medium text-gray-500 dark:text-gray-200">
            Start Weight
        </x-filament::card.heading>
        @php
            $the_weight = App\Models\User::find($this->record->id);
            $last_weight = $the_weight->lastWeight['weight'];
            $start_weight = $the_weight->startWeight['weight'];
            
        @endphp
    
        <span class="text-3xl">{{ $start_weight }}</span>
    </x-filament::card>

    <x-filament::card class="p-6">
        <x-filament::card.heading class="flex items-center space-x-2 rtl:space-x-reverse text-sm font-medium text-gray-500 dark:text-gray-200">
            BMI
        </x-filament::card.heading>

        @php
            $bmi = round($last_weight /pow($this->record->tall/100,2) ,2)
        @endphp
    
        <span class="text-3xl">{{ $bmi }}</span>
    </x-filament::card>

    <x-filament::card class="p-6">
        <x-filament::card.heading class="flex items-center space-x-2 rtl:space-x-reverse text-sm font-medium text-gray-500 dark:text-gray-200">
            Last Weight
        </x-filament::card.heading>
        
    
        <span class="text-3xl">{{$last_weight}}</span>
    </x-filament::card>
    
    
</div>




</x-filament::page>
