<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;


class UserWeight extends Model
{
    use HasFactory;

    protected $fillable = ['weight'];

    protected $casts = [
        'weight' => 'integer',
    ];

    public function user()
    {
        return $this->belongsTo(
            User::class
        );
    }

    public function scopeUser(Builder $builder): Builder
    {
        return $builder->whereBelongsTo(Auth()->user()->user);
        
    }
}
