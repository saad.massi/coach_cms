<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MealPlan extends Model
{
    use HasFactory;


    protected $fillable = [
        'name',
        'body',
        'user_id'
    ];

    protected $casts = [
        'body' => 'array',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
