<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Filament\Models\Contracts\FilamentUser;

class User extends Authenticatable implements FilamentUser
{
    use HasApiTokens, HasFactory, Notifiable;

    public function canAccessFilament(): bool
    {
        return str_ends_with($this->email, '@admin.com') && $this->hasVerifiedEmail();
    }


    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'birthday',
        'tall',
        'gender',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function mealplans()
    {
        return $this->hasMany(
            MealPlan::class,
            'user_id',
        );
    }

    public function workoutplans()
    {
        return $this->hasMany(
            WorkoutPlan::class,
            'user_id',
        );
    }

    public function weights()
    {
        return $this->hasMany(
            UserWeight::class,
            'user_id',
        );
    }

    public function lastWeight()
    {
        return $this->hasOne(UserWeight::class)->latest('created_at');
    }

    public function startWeight()
    {
        return $this->hasOne(UserWeight::class)->oldestOfMany();
    }

    public function customers()
    {
        return $this->hasMany(
            Customer::class,
            'user_id',
        );
    }

    /**
     * Get all of the userimages for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userimages(): HasMany
    {
        return $this->hasMany(UserImage::class, 'user_id');
    }

    
}
