<?php

namespace App\Filament\Widgets;

use App\Models\User;
use App\Models\UserWeight;
use Filament\Widgets\StatsOverviewWidget as BaseWidget;
use Filament\Widgets\StatsOverviewWidget\Card;
use Illuminate\Database\Eloquent\Model;


class StatsOverview extends BaseWidget
{
    public ?Model $record = null;

    protected function getCards(): array
    {
        return [
            Card::make('Users', User::query()->count()),
            Card::make('Users', $this->record),
            Card::make('Users avg weight', function ()
                {
                    $user_weight = UserWeight::get('weight')->toArray();
                    $avrege_weight = round(array_sum(array_column($user_weight, 'weight'))/count($user_weight), 2) ;
                    return $avrege_weight;
                })->descriptionIcon('heroicon-s-trending-up'),
            Card::make('Users', User::query()->count())
                ->description('32k increase')
                ->descriptionIcon('heroicon-s-trending-up')->color('danger')->chart([7, 2, 10, 3, 15, 4, 17]),
        ];
    }
}
