<?php

namespace App\Filament\Resources\BlogPostResource\Widgets;

use Filament\Widgets\Widget;

class BlogPostsOverview extends Widget
{
    protected static string $view = 'filament.resources.blog-post-resource.widgets.blog-posts-overview';
}
