<?php

namespace App\Filament\Resources\UserResource\RelationManagers;

use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\RelationManagers\HasManyRelationManager;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\KeyValue;

class MealplansRelationManager extends HasManyRelationManager
{
    protected static string $relationship = 'mealplans';

    protected static ?string $recordTitleAttribute = 'used_id';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('name')->required(),
                //Forms\Components\TextInput::make('user_id'),
                Section::make('Meals')
                    ->schema([
                        KeyValue::make('body')
                            ->keyLabel('Property name')
                            ->valueLabel('Property value')
                            ->label('Meals')
                            ->keyPlaceholder('Property name')
                ])
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('name')->searchable()->sortable(),
                Tables\Columns\TextColumn::make('created_at')->searchable()->sortable(),
            ])
            ->filters([
                //
            ]);
    }

    protected function mutateFormDataBeforeCreate(array $data): array
    {
        $data['user_id'] = auth()->id();
    
        return $data;
    }
}
