<?php

namespace App\Filament\Resources\UserResource\Widgets;

use Filament\Widgets\StatsOverviewWidget as BaseWidget;
use Filament\Widgets\StatsOverviewWidget\Card;
use App\Models\User;
use App\Models\UserWeight; 

class StatsOverview extends BaseWidget 
{
 

    protected function getCards(): array
    {
    
        return [
            Card::make('Start weight', 45),

            Card::make('BMI', function ()
                {
                    $user_weight = UserWeight::get('weight')->toArray();
                    $avrege_weight = round(array_sum(array_column($user_weight, 'weight'))/count($user_weight), 2) ;
                    return $avrege_weight;
                })->descriptionIcon('heroicon-s-trending-up'),

            Card::make('Last weight', User::query()->count())
                ->description('32k increase')
                ->descriptionIcon('heroicon-s-trending-up')->color('danger')->chart([7, 2, 10, 3, 15, 4, 17]),
            
        ];
        
    }
}
