<?php

namespace App\Filament\Resources\UserResource\Pages;

use App\Filament\Resources\UserResource;
use Filament\Resources\Pages\ViewRecord;
use App\Filament\Resources\UserResource\Widgets\StatsOverview;


class ViewUser extends ViewRecord
{
    protected static string $resource = UserResource::class;

    protected static string $view = 'filament.resources.user-resource.pages.view-user';

    protected function getHeaderWidgets(): array
    {
        return [
            //StatsOverview::class
        ];
    }
    
} 
 

