<?php

namespace App\Filament\Resources;

use App\Filament\Resources\MealPlanResource\Pages;
use App\Filament\Resources\MealPlanResource\RelationManagers;
use App\Models\MealPlan;
use App\Models\User;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Forms\Components\Repeater;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\KeyValue;

class MealPlanResource extends Resource
{
    protected static ?string $model = MealPlan::class;

    protected static ?string $navigationIcon = 'heroicon-o-document-add';

    protected static ?int $navigationSort = 2;

    //protected static ?string $label = 'customer';


    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('name')->required(),
                Forms\Components\BelongsToSelect::make('user_id')
                    ->relationship('user', 'name')
                    ->label('User')
                    ->required()
                    ->searchable(),
                /* Repeater::make('body')
                ->schema([
                    Select::make('role')
                        ->options([
                            '__("first_meal")' => 'first',
                            '__("second_meal")' => 'Socnd',
                            '__("third_meal")' => 'Owner',
                        ]),
                        TextInput::make('name')->required(),
                ]), */

                Section::make('Meals')
                    ->schema([
                        KeyValue::make('body')
                            ->keyLabel('Property name')
                            ->valueLabel('Property value')
                            ->label('Meals')
                            ->keyPlaceholder('Property name')
                ])
                          
            ])->columns(1);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('user.name')
                    ->searchable()
                    ->sortable()
                    ->label('for user'),
                Tables\Columns\TextColumn::make('name'),
                Tables\Columns\TextColumn::make('created_at')->sortable(),
            ])
            ->filters([
                //
            ]);
    }

    public static function getRelations(): array
    {
        return [

        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListMealPlans::route('/'),
            'create' => Pages\CreateMealPlan::route('/create'),
            'view' => Pages\ViewMealPlan::route('/{record}'),
            'edit' => Pages\EditMealPlan::route('/{record}/edit'),
        ];
    }

    public static function getWidgets(): array
{
    return [
        Widgets\CustomerOverview::class,
    ];
}
}
