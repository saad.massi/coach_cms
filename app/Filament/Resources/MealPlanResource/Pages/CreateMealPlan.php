<?php

namespace App\Filament\Resources\MealPlanResource\Pages;

use App\Filament\Resources\MealPlanResource;
use Filament\Resources\Pages\CreateRecord;

class CreateMealPlan extends CreateRecord
{
    protected static string $resource = MealPlanResource::class;
}
