<?php

namespace App\Filament\Resources\MealPlanResource\Pages;

use App\Filament\Resources\MealPlanResource;
use App\Models\MealPlan;
use Filament\Forms\Components\Builder;
use Filament\Resources\Pages\Page;

class ViewMealPlan extends Page
{
    protected static string $resource = MealPlanResource::class;

    protected static string $view = 'filament.resources.meal-plan-resource.pages.view-meal-plan';

    protected function mutateFormDataBeforeFill(array $data): array
        {
            $data['user_id'] = auth()->id();
        
            return $data;
        }

}

